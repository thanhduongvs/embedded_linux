# Yocto udoo neo

## Quick Start Guide  
1. clone repository:  
- `git clone https://gitlab.com/embedded-yocto/udoo-neo-bsp.git -b morty` 
- *$* `cd udoo-neo-bsp`  
2. build image:
- setup environment:  
*udoo-neo-bsp$* `MACHINE=udoovn DISTRO=fslc-framebuffer source setup-environment build`
- download package:  
*udoo-neo-bsp/build$* `bitbake -c fetchall udoo-image-qt5`
- build:  
*udoo-neo-bsp/build$* `bitbake -k udoo-image-qt5` hoặc `bitbake udoo-image-qt5` .
- Có nghĩa là bitbake sẽ tìm đến file udoo-image-qt5.bb để build image. file udoo-image-qt5.bb tương đương file main.c.  
- Đối số `-k` có ý nghĩa khi build có lỗi thì sẽ bỏ qua lỗi đó và tiếp tục build, nếu không có `-k` thì khi gặp lỗi sẽ dừng lại không build tiếp nữa.
3. Burning the image to the SD-Card:  
- *udoo-neo-bsp/build$* `umount /dev/mmcblk0`  (`umount /dev/<disk>`)  
- *udoo-neo-bsp/build$* `bzcat tmp/deploy/images/udoovn/udoo-image-qt5-udoovn.wic.bz2 | sudo dd of=/dev/mmcblk0 bs=32M`  (`sudo dd of=/dev/<disk> bs=32M`)

## edit
1. edit wifi:  
`udoo-neo-bsp/sources/meta-udoo/recipes-connectivity/wifi/files/wpa_supplicant1.conf`
2. edit user:  
`udooer@udooer`
3. chỉnh thư mục download vào thư mục udoo-neo-bsp/download gõ lệnh, để chọn thư mục download:
`ln -s /home/download`

`bitbake comman list`

What is metadata?
- Recipes (*.bb): tương đương như file .c
- PackageGroups (speacial *.bb): nhóm build
- Classes (*.bbclass): chứa những tính năng chung
- Configuration (*.conf): file cấu hình
- Append files (*.bbappend): file mở rộng
- Include files(*.inc): tương đương file .h

SRC_URI: khai báo đường dẫn file download
DEPENDS: file phụ thuộc vào
EXTRA_OECONF, : cấu hình thêm cho trình biên dịch
FILES_* : đưa file out put vào 

để edit biến MACHINE vào file build/config/local.config 
