# Yocto Project QEMU

![Build Status](https://img.shields.io/badge/%40thanhduongvs-yocto-green.svg)

## :warning: [- Để build yocto cần dung lượng ổ cứng ít nhất 100GB -]

## Bước cơ bản  

1. Tải file source code:
- :computer: *$* `mkdir yocto`
- :computer: *$* `cd yocto`
- :computer: *yocto$* `wget http://downloads.yoctoproject.org/releases/yocto/yocto-2.1.1/poky-krogoth-15.0.1.tar.bz2`
- :computer: *yocto$* `tar xf poky-krogoth-15.0.1.tar.bz2`  

2. Build yocto:
- :computer: *yocto$* `source poky-krogoth-15.0.1/oe-init-build-env`  
- :pushpin: Mặc định khi build sẽ chọn **MACHINE = "qemux86"** (máy ảo nhân x86). Để chọn **MACHINE = "qemuarm"** (máy ảo nhân arm), có 2 cách:
  - Thay vì gõ lệnh `source poky-krogoth-15.0.1/oe-init-build-env` như trên, bây giờ ta gõ lại thành `MACHINE=qemuarm source poky-krogoth-15.0.1/oe-init-build-env`
  - Vào thư mục **_yocto/build/conf_** mở file **_local.conf_**, và tìm đoạn *MACHINE ??= "qemux86"* thay thế bằng *MACHINE ??= "qemuarm"*.  
- :pushpin: Giả sử sau này bạn build yocto cho board *beaglebone black*, *udoo neo*, *raspberry pi* thì biến **MACHINE** lúc này là  *MACHINE = "beaglebone-back"*, *MACHINE = "udoo-neo"*, *MACHINE = "raspberry-pi"*
- :computer: *yocto/build$* `bitbake -c fetchall core-image-minimal`
- :pushpin: Bước này yocto sẽ tải về các package phục vụ cho việc buid.  
- :computer: *yocto/build$* `bitbake -k core-image-minimal`
- :pushpin: Bước này yocto sẽ build image.  
3. Chạy máy ảo:  
- :computer: *yocto/build$* `runqemu qemux86`  
- :pushpin: Để đăng nhập máy ảo gõ `root`.  

## Tạo lớp mẫu

1. Tạo lớp mẫu:  
- :computer: *$* `cd yocto`  
- :computer: *yocto$* `source poky/oe-init-build-env build`  
- :pushpin: Hai bước trên chỉ thực hiện lại khi tắt terminal  
- :computer: *yocto/build$* `yocto-layer create example`  
- :pushpin: Lệnh này sẽ tạo ra thư mục có tên là **meta-example** trong thư mục `build`, do hiện tại đang đứng trong thư mục `build`.
- :computer: *yocto/build$* `mv -f meta-example/ ../poky-krogoth-15.0.1/`  
- :pushpin: Duy chuyển thư mục vừa tạo **meta-example** vào thư mục **poky-krogoth-15.0.1**

2. Chỉnh sửa file conf:  
- :computer: *(yocto/build$)* `bitbake -f example`
- :pushpin: Sau khi thực hiện bước trên, thì sẽ báo lỗi không tìm thấy module **example**.  
- :pencil: Do đó vào thư mục **yocto/build/conf** mở file **bblayers.conf** rồi thêm dòng `/home/thanhduong/working/yocto-test/poky-krogoth-15.0.1/meta-example \` là đường dẫn tới thư mục **meta-example**. Đường dẫn trên còn tùy thuộc vào máy mỗi người.
- :computer: *(yocto/build$)* `bitbake -f example`
- :pushpin: Lệnh ở trên được gõ lại, để kiểm tra còn lỗi nữa không.

3. Chỉnh sửa filecore-image-minimal:  
- :pushpin: Bước làm trên chỉ build **meta-example** chứ khi gõ lệnh `bitbake -k core-image-minimal` thì **meta-example** chưa được build trong kernel.(Nói chung là build ra vẫn chưa có `meta-example` trong đó).  
- :pushpin: Do đó cần chỉnh sửa lại **core-image-minimal**. Để chỉnh **core-image-minimal** cần tìm file **core-image-minimal.bb**. Vào thư mục *yocto/poky-krogoth-15.0.1$* gõ lệnh `find -name "core-image-minimal.bb"` để tìm vị trí file để chỉnh sửa. Kết quả là nằm ở đường dẫn `/poky-krogoth-15.0.1/meta/recipes-core/images/core-image-minimal.bb`.  
- :pencil: Mở file **core-image-minimal.bb** và thêm dòng lệnh `IMAGE_INSTALL += " example"` (hoặc `IMAGE_INSTALL_append = " example"`). Quy tắc là `IMAGE_INSTALL += "<dấu_cách><tên_module>"`.  

4. Build lại image:  
- :computer: *(yocto/build$)* `bitbake -k core-image-minimal`
- :computer: *(yocto/build$)* `runqemu qemux86`  
- :pushpin: Để đăng nhập máy ảo gõ `root`.
- :computer: Trên terminal máy ảo, gõ `helloworld` để chạy ví dụ mẫu, sẽ in ra **Hello World!**.
- :pushpin: *helloword* là ví dụ mẫu sẵn có trong thư mục meta-example. Được tạo ra khi gõ lệnh `yocto-layer create example`.  
- :pushpin: Trên máy ảo, bây giờ **helloworld** nằm trong thư mục */usr/bin/*. Bản chất *helloworld* bây giờ nó tương tự như các lệnh thông thường như *cd, pwd, ls, cp, mv*.
- :pushpin: `bitbake -e example | grep "^S="` xem đường dẫn example. Phần này để ghi chú. Không có trong bước này.

## Tự động chạy ví dụ  

1. Đặt vấn đề:  
- :pushpin: Ở bước trên, ví dụ mẫu chỉ chạy khi gõ lệnh `helloworld`.  
- :pushpin: Bây giờ muốn chạy `hello world` ngay từ đầu mà không cần gõ lệnh sẽ như thế nào.  
2. Chỉnh sửa:  
- :pencil: Vào thư mục *poky-krogoth-15.0.1/meta-example/recipes-example/example/example-0.1* (là thư mục chứa source code của meta-example) tạo 1 file  script có tên *example-script* với nội dung như bên dưới:  
```bash
#!/bin/sh

case $1 in
	start)
		/usr/bin/helloworld
		;;

	stop)
		killall helloworld
		;;

	restart)
		$0 stop
		$0 start
		;;
	*)
		echo "Usage: $0 [start|stop|restart]"
		;;
esac

```  
- :pencil: Vào thư mục *poky-krogoth-15.0.1/meta-example/recipes-example/example/* để chỉnh sửa file *example_0.1.bb*. Thêm các dòng:  
`inherit update-rc.d`  
`INITSCRIPT_NAME = "example-script"`  
`INITSCRIPT_PARAMS = "defaults 90 10"`  
`SRC_URI += "file://example-script"`  
`FILES_${PN} += "/etc/init.d/*"`  
- :pushpin: Sau khi thêm các dòng trên thì file *example_0.1.bb* có nội dung như sau:  
```yocto
#
# This file was derived from the 'Hello World!' example recipe in the
# Yocto Project Development Manual.
#

SUMMARY = "Simple helloworld application"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit update-rc.d
INITSCRIPT_NAME = "example-script"
INITSCRIPT_PARAMS = "defaults 90 10"

SRC_URI = "file://helloworld.c"
SRC_URI += "file://example-script"
SRC_URI += "file://01-example.patch"

S = "${WORKDIR}"

do_compile() {
	     ${CC} helloworld.c -o helloworld
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 helloworld ${D}${bindir}

	     # Install startup script
	     install -d ${D}/etc/init.d
	     install -m 0755 example-script ${D}/etc/init.d
}

# Add the startup script to the list of files to be packaged.  Any files
# that are installed but not packaged will cause a warning to be printed
# during the bitbake process.
FILES_${PN} += "/etc/init.d/*"
```
3. Build lại:  
- :computer: *yocto/build$* `bitbake -f example`
- :computer: *yocto/build$* `bitbake -k core-image-minimal`  
- :computer: *yocto/build$* `runqemu qemux86`   
- :pushpin: Trong lúc boot, trước khi gõ `root` để đăng nhập. Thì sẽ xuất hiện dòng chữ **Hello World!**. Tức là ví dụ mẫu example đã tự chạy mà không cần gõ lệnh `helloworld`.  
- :pushpin: Đăng nhập rồi gõ lệnh `ps -l | grep helloworld` để kiểm tra pid của *helloworld* vừa chạy.

## Tạo patch file  

1. Đặt vấn đề:  
- :pushpin: Ở ví dụ bước trên sẽ in ra **Hello World!**.
- :pushpin: Bây giờ muốn in ra dòng  **Hello Van Son!**. Mà không phải chỉnh source code thì phải làm như thế nào.  
2. Tạo patch file:  
- :computer: *yocto/build$* `bitbake -e example | grep "^S="`
- :pushpin: Để tìm đường dẫn file build của *example*. kết quả là *build/tmp/work/i586-poky-linux/example/0.1-r0*.
- :pushpin: Trong thư mục *0.1-r0* có file *helloworld.c* (khúc này nói thêm - thực chất file này được copy từ thư mục *poky-krogoth-15.0.1/meta-example/recipes-example/example/example-0.1* để cho quá trình build). 
- :pushpin: Copy file *helloworld.c* (chỉ copy chưa paste vô đâu hết).  
- :pushpin: Trở lại trước đó một thư mục (tức *build/tmp/work/i586-poky-linux/example/*).
- :pushpin: Tạo 2 thư mục có tên *a* và *b*. Paste file *helloworld.c* vào cả 2 thư mục này. 
- :pushpin: Vào thư mục *b* mở file *helloworld.c* và chỉnh sửa dòng *printf("Hello World!\n");* thành *printf("Hello Van Son!\n");*.  
- :pushpin: Trở lại thư mục trước đó (tức *build/tmp/work/i586-poky-linux/example/*) gõ lệnh `diff -rupN a/helloworld.c b/helloworld.c` để xem khác biệt giữa 2 file.  
- :computer: Gõ lệnh `diff -rupN a b > 0001-example.patch` để tạo file patch.  
- :pushpin: Move file 0001-example.patch vào thư mục */poky-krogoth-15.0.1/meta-example/recipes-example/example/example-0.1* .  
- :pencil: Mở file *example_0.1.bb* trong thư mục */poky-krogoth-15.0.1/meta-example/recipes-example/example* và thêm dòng `SRC_URI += "file://0001-example.patch"` .  
3. Build lại:  
- :computer: *yocto/build$* `bitbake -f example`
- :computer: *yocto/build$* `bitbake -k core-image-minimal`  
- :computer: *yocto/build$* `runqemu qemux86`   
- Để ý trong lúc build có in ra dòng *Hello Van Son!* không.  
4. Ghi chú: 
- :pushpin: Mục đích việc tạo 2 thư mục a và b là để tạo patch file.  
- :pushpin: Lệnh `diff -rupN a b > 0001-example.patch`, không chỉ so sánh file *hellworld.c* mà so sánh cả 2 thư mục a và b. Giả sử trong 2 thư mục đó có nhiều file khác nhau, thì file *0001-example.patch* tạo ra sẽ so sánh nhiều file trong 2 thư mục đó.  

## Add driver example
poky-krogoth-15.0.1/meta-skeleton/recipes-kernel/hello-mod

